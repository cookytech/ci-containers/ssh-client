FROM alpine:latest
# Upgrading the distro to latest packages
RUN apk add openssh-client
RUN eval $(ssh-agent -s)
# RUN echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh
CMD sh
